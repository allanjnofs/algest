import {Component, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';
import {Car} from './entities/interfaces/car';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MessageService]
})
export class AppComponent implements OnInit {
  title = 'tt';
  cars: Car[];
  cols: any[];
  selectedCar1: Car;

  constructor(private messageService: MessageService) {
  }


  ngOnInit(): void {
    this.cars = [{'brand': 'VW', 'year': 2012, 'color': 'Orange', 'vin': 'dsad231ff'},
      {'brand': 'Audi', 'year': 2011, 'color': 'Black', 'vin': 'gwregre345'},
      {'brand': 'Renault', 'year': 2005, 'color': 'Gray', 'vin': 'h354htr'},
      {'brand': 'BMW', 'year': 2003, 'color': 'Blue', 'vin': 'j6w54qgh'},
      {'brand': 'Mercedes', 'year': 1995, 'color': 'Orange', 'vin': 'hrtwy34'},
      {'brand': 'Volvo', 'year': 2005, 'color': 'Black', 'vin': 'jejtyj'},
      {'brand': 'Honda', 'year': 2012, 'color': 'Yellow', 'vin': 'g43gr'},
      {'brand': 'Jaguar', 'year': 2013, 'color': 'Orange', 'vin': 'greg34'},
      {'brand': 'Ford', 'year': 2000, 'color': 'Black', 'vin': 'h54hw5'},
      {'brand': 'Fiat', 'year': 2013, 'color': 'Red', 'vin': '245t2s'}];

    this.cols = [
      {field: 'vin', header: 'Vin'},
      {field: 'year', header: 'Year'},
      {field: 'brand', header: 'Brand'},
      {field: 'color', header: 'Color'}
    ];
  }

  showToast($event): void {
    console.log($event);
    this.messageService.add({severity: 'success', summary: 'Service Message', detail: 'Via MessageService'});
  }

  limpar() {
    this.messageService.clear();
  }
}

