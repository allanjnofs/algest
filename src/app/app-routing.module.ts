import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LancamentoListarComponent} from './componentes/lancamento/lancamento-listar/lancamento-listar.component';
import {LancamentoInserirComponent} from './componentes/lancamento/lancamento-inserir/lancamento-inserir.component';

const routes: Routes = [
  {path: 'lancamentos', component: LancamentoListarComponent},
  {path: 'lancar', component: LancamentoInserirComponent},
  {path: 'editar/:id', component: LancamentoInserirComponent},
  {path: '', redirectTo: '/lancamentos', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
