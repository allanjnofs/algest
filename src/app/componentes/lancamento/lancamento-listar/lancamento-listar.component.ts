import {Component, OnInit} from '@angular/core';
import {LancamentoService} from '../../../services/lancamento.service';
import {Lancamento} from 'src/app/entidades/Lancamento';
import {Router} from '@angular/router';
import {Confirmation, ConfirmationService, Message, MessageService} from 'primeng/api';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-lancamento-listar',
  templateUrl: './lancamento-listar.component.html',
  styleUrls: ['./lancamento-listar.component.css']
})
export class LancamentoListarComponent implements OnInit {

  //#region Atributos
  lancamentoSelecionado: Lancamento;
  colunas: any[];
  lancamentos: Lancamento[];
  tipoLancamento: String = 'null';
  dataInicial: Date = new Date(2019, 9, 1);
  dataFinal: Date = new Date();
  en: any;
  carregando: Boolean;
  //#endregion Atributos

  //#region Construtor
  constructor(public service: LancamentoService, private router: Router, private confirmationService: ConfirmationService, private messageService: MessageService) {
  }

  //#endregion Construtor

  //#region Metodos
  ngOnInit() {
    this.colunas = [
      {field: 'id', header: 'Id'},
      {field: 'descricao', header: 'Descrição'},
      {field: 'data', header: 'Data'},
      {field: 'dataSistema', header: 'Lançamento'},
      {field: 'valor', header: 'Valor'},
      {field: 'isCredito', header: 'C/D'}
    ];
    this.carregarLancamentos();
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sext', 'Sáb'],
      dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      today: 'Today',
      clear: 'Clear',
      dateFormat: 'mm/dd/yy',
      weekHeader: 'Wk'
    };
  }

  carregarLancamentos(): void {
    try {
      this.carregando = true;
      this.service.listarLancamentos(this.tipoLancamento, this.dataInicial, this.dataFinal).subscribe((data: Lancamento[]) => {
        this.lancamentos = data;
        this.carregando = false;
      }, (erro: HttpErrorResponse) => {
        console.log(erro);
        this.messageService.add({severity: 'error', detail: erro.message, summary: 'Erro'});
        this.carregando = false;
      });
    } catch (erro) {
      console.log(erro);
      this.messageService.add({severity: 'error', detail: erro.message, summary: 'Erro'});
      this.carregando = false;
    }
  }

  onFilter(): void {
    this.carregarLancamentos();
  }

  onNovo(): void {
    this.router.navigate(['/lancar']);
  }

  onRemover() {
    let confirmation: Confirmation = {};
    confirmation.message = `Tem certeza que deseja remover o lançamento ${this.lancamentoSelecionado.id}?`;
    confirmation.accept = () => {
      this.service.removerLancamento(this.lancamentoSelecionado).subscribe(
        (data: Lancamento) => {
          this.carregarLancamentos();
        },
        (ex: HttpErrorResponse) => {
          let message: Message = {};
          message.severity = 'error';
          message.summary = 'Erro';
          message.detail = 'Não foi possivel remover esse lançamento.';
          this.messageService.add(message);
        }
      );
    };
    this.confirmationService.confirm(confirmation);
  }

  onEditar() {
    this.router.navigate(['/editar', this.lancamentoSelecionado.id]).catch(reason => {
    });
  }

  //#endregion Metodos


}
