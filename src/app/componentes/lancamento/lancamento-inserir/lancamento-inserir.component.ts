import {filter, map, switchMap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Lancamento} from '../../../entidades/Lancamento';
import {LancamentoService} from '../../../services/lancamento.service';
import {MessageService} from 'primeng/api';
import {from, Observable, of} from 'rxjs';

@Component({
  selector: 'app-lancamento-inserir',
  templateUrl: './lancamento-inserir.component.html',
  styleUrls: ['./lancamento-inserir.component.css']
})
export class LancamentoInserirComponent implements OnInit {

  lancamento: Lancamento;

  message: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private service: LancamentoService, public messageService?: MessageService) {
    this.lancamento = new Lancamento();
    this.lancamento.isCredito = true;
    this.lancamento.data = new Date();
  }

  ngOnInit() {
    let result: Observable<Lancamento> = this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) => {
        // (+) before `params.get()` turns the string into a number
        let id: Number = +params.get('id');
        if (id > 0) {
          return this.service.findById(id);
        } else {
          /* ======================================================================================================================================================== */
          let numbers: Observable<number> = of(1, 2, 3);
          let squareOperator = map((val: number) => {
            return (Number)(val * val);
          });
          let squareNumbers = squareOperator(numbers);

          let filterSquareNumbers = numbers.pipe(filter(n => n % 2 == 0), map(n => n * n), map(n => 'Allan: ' + n)).subscribe(val => console.log(val));


          from<Lancamento[]>([this.lancamento, null, null, null, null]);
          /* ======================================================================================================================================================== */
          return of<Lancamento>(this.lancamento);
        }
      })
    );
    result.subscribe((lancamento: Lancamento) => {
      lancamento.dataSistema = new Date(lancamento.dataSistema);
      lancamento.data = new Date(new Date(lancamento.data.toString()).getTime() + new Date().getTimezoneOffset() * 60 * 1000);
      this.lancamento = lancamento;
    }, (ex: HttpErrorResponse) => {
      console.log(ex);
    });
  }

  onSalvar(): void {
    this.lancamento.data = new Date(new Date(this.lancamento.data.toString()).getTime() - new Date().getTimezoneOffset() * 60 * 1000);
    const obs: Observable<Lancamento> = this.lancamento.id > 0 ? this.service.atualizarLancamento(this.lancamento) : this.service.inserirLancamento(this.lancamento);
    obs.subscribe((data: Lancamento) => {
      this.messageService.add({severity: 'success', summary: 'Sucesso', detail: 'Lançamento inserido com sucesso!'});
      this.router.navigate(['/lancamentos']).catch();
    }, (erro: HttpErrorResponse) => {
      if (erro.status >= 500) {
        this.messageService.add({severity: 'error', summary: 'Erro', detail: erro.error});
      } else if (erro.status >= 400) {
        this.messageService.add({severity: 'warn', summary: 'Aviso', detail: erro.error});
      } else {
        this.messageService.add({severity: 'error', summary: 'Erro', detail: 'Erro no Servidor'});
      }
    });
  }

  onCancelar(): void {
    this.router.navigate(['/lancamentos']).catch(reason => {
    });
  }

}
