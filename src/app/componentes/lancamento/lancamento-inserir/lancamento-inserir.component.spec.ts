import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancamentoInserirComponent } from './lancamento-inserir.component';

describe('LancamentoInserirComponent', () => {
  let component: LancamentoInserirComponent;
  let fixture: ComponentFixture<LancamentoInserirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancamentoInserirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancamentoInserirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
