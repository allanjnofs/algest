import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastModule} from 'primeng/toast';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {
  CalendarModule,
  CardModule,
  CheckboxModule, ConfirmationService,
  ConfirmDialogModule, DialogModule,
  InputTextModule, MessageService,
  PanelModule,
  RadioButtonModule,
  TreeTableModule
} from 'primeng/primeng';
import {LancamentoListarComponent} from './componentes/lancamento/lancamento-listar/lancamento-listar.component';
import {LancamentoInserirComponent} from './componentes/lancamento/lancamento-inserir/lancamento-inserir.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {DatePipe} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    LancamentoListarComponent,
    LancamentoInserirComponent
  ],
  imports: [
    /*Angular*/
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    /*Prime*/
    ToastModule,
    ButtonModule,
    TableModule,
    TreeTableModule,
    RadioButtonModule,
    CalendarModule,
    CardModule,
    PanelModule,
    CheckboxModule,
    InputTextModule,
    ConfirmDialogModule,
    DialogModule
  ],
  providers: [DatePipe, ConfirmationService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
