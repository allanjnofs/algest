import {Injectable} from '@angular/core';
import {Lancamento} from '../entidades/Lancamento';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DatePipe} from '@angular/common';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LancamentoService {

  private lancamentos: Lancamento[];

  constructor(private httpClient: HttpClient, private datePipe: DatePipe) {
    debugger;
  }

  /**
   *  Lista todos os Lançamentos.
   */
  listarLancamentos(tipoLancamento: String = 'null', dataInicial: Date, dataFinal: Date): Observable<Lancamento[]> {
    let httpParams: HttpParams = new HttpParams();
    const url = 'http://localhost:8080/algest/rest/lancamentos';
    httpParams = httpParams.set('dataInicial', this.datePipe.transform(dataInicial, 'yyyy-MM-dd')).set('dataFinal', this.datePipe.transform(dataFinal, 'yyyy-MM-dd'));
    if (tipoLancamento + '' !== 'null') {
      httpParams = httpParams.set('tipoLancamento', tipoLancamento.valueOf());
    }
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json').set('Accept-Charset', 'utf-8');
    return this.httpClient.get<Lancamento[]>(url, {headers: httpHeaders, params: httpParams}).pipe(map((result: Lancamento[]) => {
      debugger;
      this.lancamentos = result;
      this.lancamentos.forEach(element => {
        element.dataSistema = new Date(element.dataSistema);
        element.data = new Date(new Date(element.data.toString()).getTime() + new Date().getTimezoneOffset() * 60 * 1000);
      });
      return result;
    }));
  }

  /**
   * Insere um novo Lançamento no Banco de Dados.
   */
  inserirLancamento(lancamento: Lancamento): Observable<Lancamento> {
    const url = 'http://localhost:8080/algest/rest/lancamentos';
    return this.httpClient.post<Lancamento>(url, lancamento);
  }

  removerLancamento(lancamento: Lancamento): Observable<Lancamento> {
    let url = 'http://localhost:8080/algest/rest/lancamentos/';
    url = `${url}${lancamento.id}`;
    return this.httpClient.delete<Lancamento>(url);
  }

  findById(id: Number): Observable<Lancamento> {
    let url = 'http://localhost:8080/algest/rest/lancamentos/';
    url = `${url}${id}`;
    return this.httpClient.get<Lancamento>(url);
  }

  atualizarLancamento(lancamento: Lancamento): Observable<Lancamento> {
    const url = 'http://localhost:8080/algest/rest/lancamentos';
    return this.httpClient.put<Lancamento>(url, lancamento);

  }
}
