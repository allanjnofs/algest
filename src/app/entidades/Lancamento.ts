export class Lancamento {
	public id: Number;
	public valor: Number;
	public data: Date;
	public dataSistema: Date;
	public isCredito: Boolean;
	public descricao?: String;

	constructor() {
	}

}
